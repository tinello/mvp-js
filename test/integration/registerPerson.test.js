/* global jest */
/* global describe */
/* global beforeEach */
/* global document */
/* global it */
/* global expect */
jest.mock('./../../src/registerPerson/registerPersonClient', () => ({
  registerPerson: jest.fn(),
}));

const fs = require('fs');
const path = require('path');
const view = require('./../../src/registerPerson/registerPersonView');
const client = require('./../../src/registerPerson/registerPersonClient');
const presenter = require('./../../src/registerPerson/registerPersonPresenter');

function loadTemplate(viewPath, onLoad) {
  const filePath = path.join(__dirname, viewPath);
  fs.readFile(filePath, { encoding: 'utf-8' }, (error, data) => {
    if (!error) {
      onLoad(data);
    } else {
      console.log(error);
    }
  });
}

describe('register person', () => {
  beforeEach((done) => {
    loadTemplate('../../src/index.html', (html) => {
      document.body.innerHTML = html;
      presenter(view(), client);
      done();
    });
  });

  it('loads the markup', () => {
    expect(document.querySelector('h1')).not.toBeNull();
  });

  it('shows success message when register person', () => {
    const message = document.getElementById('message');
    const registerButton = document.getElementById('registerButton');
    client.registerPerson
      .mockImplementation((request, successHandler) => {
        successHandler();
      });

    registerButton.click();

    expect(message.style.color).toBe('green');
  });

  it('shows error message when register person', () => {
    const message = document.getElementById('message');
    const registerButton = document.getElementById('registerButton');
    client.registerPerson
      .mockImplementation((request, successHandler, errorHandler) => {
        errorHandler();
      });

    registerButton.click();

    expect(message.style.color).toBe('red');
  });
});
