import view from './registerPerson/registerPersonView';
import client from './registerPerson/registerPersonClient';
import presenter from './registerPerson/registerPersonPresenter';

presenter(view(), client());
