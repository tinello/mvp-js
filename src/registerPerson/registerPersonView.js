/* global document */
function view() {
  let registerPersonRequestedHandler = () => {};

  let name;
  let surname;
  let email;
  let telephone;
  let registerButton;
  let message;

  function initialize() {
    console.log('initialize view');
    name = document.getElementById('name');
    surname = document.getElementById('surname');
    email = document.getElementById('email');
    telephone = document.getElementById('telephone');
    registerButton = document.getElementById('registerButton');
    message = document.getElementById('message');

    registerButton.addEventListener('click', () => {
      console.log('click button');
      registerPersonRequestedHandler({
        name: name.textContent,
        surname: surname.textContent,
        email: email.textContent,
        telephone: telephone.textContent,
      });
    });
  }

  function subscribeToRegisterPersonRequested(handler) {
    console.log('subscribeToRegisterPersonRequested', handler);
    registerPersonRequestedHandler = handler;
  }

  function showSuccessMessage() {
    message.innerHTML = 'Persona registrada con exito.';
    message.style.display = 'block';
    message.style.color = 'green';
  }

  function showErrorMessage() {
    message.innerHTML = 'Hubo un problema, intentelo de nuevo en unos minutos.';
    message.style.display = 'block';
    message.style.color = 'red';
  }

  initialize();

  return {
    subscribeToRegisterPersonRequested,
    showSuccessMessage,
    showErrorMessage,
  };
}

module.exports = view;
