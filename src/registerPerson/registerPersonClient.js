/* global XMLHttpRequest */
function client() {
  function registerPerson(request, successCallback, errorCallback) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = () => {
      if (this.status === 201) {
        successCallback();
      }
      if (this.status === 500) {
        errorCallback();
      }
    };
    xhttp.open('POST', 'https://jsonplaceholder.typicode.com/posts', true);
    xhttp.send(request);
  }

  return {
    registerPerson,
  };
}

module.exports = client;
