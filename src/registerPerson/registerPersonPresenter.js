function presenter(view, client) {
  console.log('presenter');
  function registerPersonRequestedHandler(personData) {
    console.log('registerPersonRequestedHandler');
    const request = {
      name: personData.name,
      surname: personData.surname,
      email: personData.email,
      telephone: personData.telephone,
    };

    function successCallback() {
      view.showSuccessMessage();
    }

    function errorHandler() {
      view.showErrorMessage();
    }

    client.registerPerson(request, successCallback, errorHandler);
  }

  view.subscribeToRegisterPersonRequested(registerPersonRequestedHandler);
}

module.exports = presenter;
